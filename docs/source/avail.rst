Available units
---------------

* Acceleration:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.acceleration_dict.keys())))


* Angles:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.angle_dict.keys())))

* Area:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.area_dict.keys())))

* Density:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.density_dict.keys())))   

* Force:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.force_dict.keys())))
   
* Inertia:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.inertia_dict.keys())))      

* Kinematic viscosity:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.kinematicviscosity_dict.keys())))
   

* Length:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.length_dict.keys())))


* Mass:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.mass_dict.keys())))

* Power:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.power_dict.keys())))
   
* Pressure:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.pressure_dict.keys())))

* Second moment of area:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.second_moment_area_dict.keys())))

* Speed:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.speed_dict.keys())))
   
   
* Time:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.time_dict.keys())))

* Volume:

.. execute_code::
   :hide_code:
   :hide_headers:

   import unitc
   print(', '.join(list(unitc.volume_dict.keys())))
